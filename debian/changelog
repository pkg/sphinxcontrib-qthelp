sphinxcontrib-qthelp (1.0.3-4+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Apr 2023 00:49:55 +0000

sphinxcontrib-qthelp (1.0.3-4) unstable; urgency=medium

  * Add a patch to fix tests with Sphinx 5 (closes: #1013387).
  * Add *.mo to debian/clean to fix build twice in a row.
  * Verify PGP signature on upstream tarballs.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 28 Jun 2022 23:21:23 +0300

sphinxcontrib-qthelp (1.0.3-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Sandro Tosi ]
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 4.6.1 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sun, 05 Jun 2022 14:09:42 -0400

sphinxcontrib-qthelp (1.0.3-2apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:12:22 +0200

sphinxcontrib-qthelp (1.0.3-2) unstable; urgency=medium

  * debian/python3-sphinxcontrib.qthelp.pyremove
    - dont ship locales/.tx directory
  * upload to unstable

 -- Sandro Tosi <morph@debian.org>  Wed, 15 Apr 2020 21:14:00 -0400

sphinxcontrib-qthelp (1.0.3-1) experimental; urgency=medium

  * Initial release; Closes: #955243

 -- Sandro Tosi <morph@debian.org>  Sat, 28 Mar 2020 22:31:21 -0400
